using UnityEngine;

    public class AntiRoll : MonoBehaviour
    {
        [SerializeField] WheelCollider _leftWheelCollider;
        [SerializeField] WheelCollider _rightWheelCollider;
        [SerializeField] float _antiRoll = 5000f;
    
        Rigidbody _rigidBody;

        void Start()
        {
            _rigidBody = GetComponent<Rigidbody>();
        }

        void FixedUpdate ()
        {
            var travelL = 1.0f;
            var travelR = 1.0f;
 
            var groundedL = _leftWheelCollider.GetGroundHit(out var hit);
            if (groundedL)
                travelL = (-_leftWheelCollider.transform.InverseTransformPoint(hit.point).y - _leftWheelCollider.radius) / _leftWheelCollider.suspensionDistance;
 
            var groundedR = _rightWheelCollider.GetGroundHit(out hit);
            if (groundedR)
                travelR = (-_rightWheelCollider.transform.InverseTransformPoint(hit.point).y - _rightWheelCollider.radius) / _rightWheelCollider.suspensionDistance;
 
            var antiRollForce = (travelL - travelR) * _antiRoll;
 
            if (groundedL)
                _rigidBody.AddForceAtPosition(_leftWheelCollider.transform.up * -antiRollForce,
                    _leftWheelCollider.transform.position);  
            if (groundedR)
                _rigidBody.AddForceAtPosition(_rightWheelCollider.transform.up * antiRollForce,
                    _rightWheelCollider.transform.position);  
        }
    }
